'use strict';

/**
 * Module dependencies
 */
var smsPolicy = require('../policies/sms.server.policy'),
  sms = require('../controllers/sms.server.controller');

module.exports = function(app) {
  // Sms Routes
  app.route('/api/sms').all(smsPolicy.isAllowed)
    .get(sms.list)
    .post(sms.create);

  app.route('/api/sms/:smId').all(smsPolicy.isAllowed)
    .get(sms.read)
    .put(sms.update)
    .delete(sms.delete);

  // Finish by binding the Sm middleware
  app.param('smId', sms.smByID);
};
