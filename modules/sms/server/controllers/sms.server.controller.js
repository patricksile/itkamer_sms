'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  SMS = mongoose.model('SMS'),
  Infobip = require('infobip-sms'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a SMS
 */
exports.create = function(req, res) {
  var sMSObject = new SMS(req.body);
  sMSObject.user = req.user;

  var sms = new Infobip('Itkamer', 'fakepwd18');

  // That is the code used to save a new SMS after being sent
  //  in real life to a person

  var sender = req.user.username;
  var recipients = [ {gsm: sMSObject.number, messagId: 'messageId'}  ];
  var msg = sMSObject.message;
  var options = { text: true };

  sms.send(sender, msg, recipients, options, function(err, response){
    console.log(err);
    console.log(response);

    if (err) {
      // In case of error we just return back an error message to the client
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      })
    } else {
      // Once the message has been sent we just save it on the database
      sMSObject.save(function(err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.jsonp(sMSObject);
        }
      });
    }
  });
};

/**
 * Show the current SMS
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var sm = req.sm ? req.sm.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  sm.isCurrentUserOwner = req.user && sm.user && sm.user._id.toString() === req.user._id.toString();

  res.jsonp(sm);
};

/**
 * Update a SMS
 */
exports.update = function(req, res) {
  var sm = req.sm;

  sm = _.extend(sm, req.body);

  sm.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(sm);
    }
  });
};

/**
 * Delete an SMS
 */
exports.delete = function(req, res) {
  var sm = req.sm;

  sm.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(sm);
    }
  });
};

/**
 * List of Sms
 */
exports.list = function(req, res) {
  SMS.find().sort('-created').populate('user', 'displayName').exec(function(err, sms) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(sms);
    }
  });
};

/**
 * SMS middleware
 */
exports.smByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'SMS is invalid'
    });
  }

  SMS.findById(id).populate('user', 'displayName').exec(function (err, sm) {
    if (err) {
      return next(err);
    } else if (!sm) {
      return res.status(404).send({
        message: 'No SMS with that identifier has been found'
      });
    }
    req.sm = sm;
    next();
  });
};
