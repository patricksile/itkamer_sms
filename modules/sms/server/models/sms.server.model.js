'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Sm Schema
 */
var SmSchema = new Schema({
  message: {
    type: String,
    default: '',
    required: 'Please fill SMS name',
    trim: true
  },
  number: {
    type: String
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('SMS', SmSchema);
