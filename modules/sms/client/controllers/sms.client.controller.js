(function () {
  'use strict';

  // Sms controller
  angular
    .module('sms')
    .controller('SmsController', SmsController);

  SmsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'smResolve'];

  function SmsController ($scope, $state, $window, Authentication, sm) {
    var vm = this;

    vm.authentication = Authentication;
    vm.sm = sm;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Sm
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.sm.$remove($state.go('sms.list'));
      }
    }

    // Save Sm
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.smForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.sm._id) {
        vm.sm.$update(successCallback, errorCallback);
      } else {
        vm.sm.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('sms.view', {
          smId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
