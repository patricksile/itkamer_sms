(function () {
  'use strict';

  angular
    .module('sms')
    .controller('SmsListController', SmsListController);

  SmsListController.$inject = ['SmsService'];

  function SmsListController(SmsService) {
    var vm = this;

    vm.sms = SmsService.query();
  }
}());
