// Sms service used to communicate Sms REST endpoints
(function () {
  'use strict';

  angular
    .module('sms')
    .factory('SmsService', SmsService);

  SmsService.$inject = ['$resource'];

  function SmsService($resource) {
    return $resource('api/sms/:smId', {
      smId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
