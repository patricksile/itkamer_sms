(function () {
  'use strict';

  angular
    .module('sms')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: 'Sms',
      state: 'sms',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'sms', {
      title: 'List Sms',
      state: 'sms.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'sms', {
      title: 'Create Sm',
      state: 'sms.create',
      roles: ['user']
    });
  }
}());
