(function () {
  'use strict';

  angular
    .module('sms')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('sms', {
        abstract: true,
        url: '/sms',
        template: '<ui-view/>'
      })
      .state('sms.list', {
        url: '',
        templateUrl: 'modules/sms/client/views/list-sms.client.view.html',
        controller: 'SmsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Sms List'
        }
      })
      .state('sms.create', {
        url: '/create',
        templateUrl: 'modules/sms/client/views/form-sm.client.view.html',
        controller: 'SmsController',
        controllerAs: 'vm',
        resolve: {
          smResolve: newSm
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Sms Create'
        }
      })
      .state('sms.edit', {
        url: '/:smId/edit',
        templateUrl: 'modules/sms/client/views/form-sm.client.view.html',
        controller: 'SmsController',
        controllerAs: 'vm',
        resolve: {
          smResolve: getSm
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Sm {{ smResolve.name }}'
        }
      })
      .state('sms.view', {
        url: '/:smId',
        templateUrl: 'modules/sms/client/views/view-sm.client.view.html',
        controller: 'SmsController',
        controllerAs: 'vm',
        resolve: {
          smResolve: getSm
        },
        data: {
          pageTitle: 'Sm {{ smResolve.name }}'
        }
      });
  }

  getSm.$inject = ['$stateParams', 'SmsService'];

  function getSm($stateParams, SmsService) {
    return SmsService.get({
      smId: $stateParams.smId
    }).$promise;
  }

  newSm.$inject = ['SmsService'];

  function newSm(SmsService) {
    return new SmsService();
  }
}());
