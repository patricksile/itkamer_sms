'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Sm = mongoose.model('Sm'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  sm;

/**
 * Sm routes tests
 */
describe('Sm CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Sm
    user.save(function () {
      sm = {
        name: 'Sm name'
      };

      done();
    });
  });

  it('should be able to save a Sm if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Sm
        agent.post('/api/sms')
          .send(sm)
          .expect(200)
          .end(function (smSaveErr, smSaveRes) {
            // Handle Sm save error
            if (smSaveErr) {
              return done(smSaveErr);
            }

            // Get a list of Sms
            agent.get('/api/sms')
              .end(function (smsGetErr, smsGetRes) {
                // Handle Sms save error
                if (smsGetErr) {
                  return done(smsGetErr);
                }

                // Get Sms list
                var sms = smsGetRes.body;

                // Set assertions
                (sms[0].user._id).should.equal(userId);
                (sms[0].name).should.match('Sm name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Sm if not logged in', function (done) {
    agent.post('/api/sms')
      .send(sm)
      .expect(403)
      .end(function (smSaveErr, smSaveRes) {
        // Call the assertion callback
        done(smSaveErr);
      });
  });

  it('should not be able to save an Sm if no name is provided', function (done) {
    // Invalidate name field
    sm.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Sm
        agent.post('/api/sms')
          .send(sm)
          .expect(400)
          .end(function (smSaveErr, smSaveRes) {
            // Set message assertion
            (smSaveRes.body.message).should.match('Please fill Sm name');

            // Handle Sm save error
            done(smSaveErr);
          });
      });
  });

  it('should be able to update an Sm if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Sm
        agent.post('/api/sms')
          .send(sm)
          .expect(200)
          .end(function (smSaveErr, smSaveRes) {
            // Handle Sm save error
            if (smSaveErr) {
              return done(smSaveErr);
            }

            // Update Sm name
            sm.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Sm
            agent.put('/api/sms/' + smSaveRes.body._id)
              .send(sm)
              .expect(200)
              .end(function (smUpdateErr, smUpdateRes) {
                // Handle Sm update error
                if (smUpdateErr) {
                  return done(smUpdateErr);
                }

                // Set assertions
                (smUpdateRes.body._id).should.equal(smSaveRes.body._id);
                (smUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Sms if not signed in', function (done) {
    // Create new Sm model instance
    var smObj = new Sm(sm);

    // Save the sm
    smObj.save(function () {
      // Request Sms
      request(app).get('/api/sms')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Sm if not signed in', function (done) {
    // Create new Sm model instance
    var smObj = new Sm(sm);

    // Save the Sm
    smObj.save(function () {
      request(app).get('/api/sms/' + smObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', sm.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Sm with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/sms/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Sm is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Sm which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Sm
    request(app).get('/api/sms/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Sm with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Sm if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Sm
        agent.post('/api/sms')
          .send(sm)
          .expect(200)
          .end(function (smSaveErr, smSaveRes) {
            // Handle Sm save error
            if (smSaveErr) {
              return done(smSaveErr);
            }

            // Delete an existing Sm
            agent.delete('/api/sms/' + smSaveRes.body._id)
              .send(sm)
              .expect(200)
              .end(function (smDeleteErr, smDeleteRes) {
                // Handle sm error error
                if (smDeleteErr) {
                  return done(smDeleteErr);
                }

                // Set assertions
                (smDeleteRes.body._id).should.equal(smSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Sm if not signed in', function (done) {
    // Set Sm user
    sm.user = user;

    // Create new Sm model instance
    var smObj = new Sm(sm);

    // Save the Sm
    smObj.save(function () {
      // Try deleting Sm
      request(app).delete('/api/sms/' + smObj._id)
        .expect(403)
        .end(function (smDeleteErr, smDeleteRes) {
          // Set message assertion
          (smDeleteRes.body.message).should.match('User is not authorized');

          // Handle Sm error error
          done(smDeleteErr);
        });

    });
  });

  it('should be able to get a single Sm that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Sm
          agent.post('/api/sms')
            .send(sm)
            .expect(200)
            .end(function (smSaveErr, smSaveRes) {
              // Handle Sm save error
              if (smSaveErr) {
                return done(smSaveErr);
              }

              // Set assertions on new Sm
              (smSaveRes.body.name).should.equal(sm.name);
              should.exist(smSaveRes.body.user);
              should.equal(smSaveRes.body.user._id, orphanId);

              // force the Sm to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Sm
                    agent.get('/api/sms/' + smSaveRes.body._id)
                      .expect(200)
                      .end(function (smInfoErr, smInfoRes) {
                        // Handle Sm error
                        if (smInfoErr) {
                          return done(smInfoErr);
                        }

                        // Set assertions
                        (smInfoRes.body._id).should.equal(smSaveRes.body._id);
                        (smInfoRes.body.name).should.equal(sm.name);
                        should.equal(smInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Sm.remove().exec(done);
    });
  });
});
