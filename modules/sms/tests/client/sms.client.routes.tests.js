(function () {
  'use strict';

  describe('Sms Route Tests', function () {
    // Initialize global variables
    var $scope,
      SmsService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _SmsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      SmsService = _SmsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('sms');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/sms');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          SmsController,
          mockSm;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('sms.view');
          $templateCache.put('modules/sms/client/views/view-sm.client.view.html', '');

          // create mock Sm
          mockSm = new SmsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Sm Name'
          });

          // Initialize Controller
          SmsController = $controller('SmsController as vm', {
            $scope: $scope,
            smResolve: mockSm
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:smId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.smResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            smId: 1
          })).toEqual('/sms/1');
        }));

        it('should attach an Sm to the controller scope', function () {
          expect($scope.vm.sm._id).toBe(mockSm._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/sms/client/views/view-sm.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          SmsController,
          mockSm;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('sms.create');
          $templateCache.put('modules/sms/client/views/form-sm.client.view.html', '');

          // create mock Sm
          mockSm = new SmsService();

          // Initialize Controller
          SmsController = $controller('SmsController as vm', {
            $scope: $scope,
            smResolve: mockSm
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.smResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/sms/create');
        }));

        it('should attach an Sm to the controller scope', function () {
          expect($scope.vm.sm._id).toBe(mockSm._id);
          expect($scope.vm.sm._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/sms/client/views/form-sm.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          SmsController,
          mockSm;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('sms.edit');
          $templateCache.put('modules/sms/client/views/form-sm.client.view.html', '');

          // create mock Sm
          mockSm = new SmsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Sm Name'
          });

          // Initialize Controller
          SmsController = $controller('SmsController as vm', {
            $scope: $scope,
            smResolve: mockSm
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:smId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.smResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            smId: 1
          })).toEqual('/sms/1/edit');
        }));

        it('should attach an Sm to the controller scope', function () {
          expect($scope.vm.sm._id).toBe(mockSm._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/sms/client/views/form-sm.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
